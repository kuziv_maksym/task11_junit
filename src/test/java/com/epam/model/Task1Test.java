package com.epam.model;

import org.junit.jupiter.api.*;
import org.junit.*;
import org.junit.jupiter.api.Test;

public class Task1Test {
    @BeforeAll
    public static void beforeClass() {
        System.out.println("Before Tests");
    }

    @BeforeEach
    public void beforeEach() {
        System.out.println("Text before this test");
    }

    @AfterEach
    public void afterEach() {
        System.out.println("Text after this test");
    }

    @AfterAll
    public static void afterClass() {
        System.out.println("After Tests");
    }

    @Test
    public void test1Task1() {
        Task1 task = new Task1();
        int[] arr = {1, 2, 3, 4, 5, 5, 5, 6, 7, 8};
        int[] arrRes = {5, 5, 5};
        int[] start = task.array(arr);
        Assert.assertArrayEquals(arrRes, start);
    }

    @Test
    @Disabled
    public void test2Task1() {
        Task1 task = new Task1();
        int[] arr = {1, 2, 3, 4, 5, 5, 5, 6, 7, 8};
        int[] arrRes = {5, 5, 5};
        Assert.assertArrayEquals(arrRes, task.array(arr));
    }

}
