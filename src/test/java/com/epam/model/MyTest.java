package com.epam.model;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MyTest {
    @InjectMocks
    MyApplication myApplication;

    @Mock
    CalcServ calcServ;


    @Test
    public void testAdd() {
        when(calcServ.add(5.0,5.0)).thenReturn(10.0);

        Assert.assertEquals(myApplication.add(5.0,5.0),10.0);
    }

    @Test
    public void testSub() {
        CalcServ calcServ = Mockito.mock(CalcServ.class);
        MyApplication myApplication = new MyApplication(calcServ);

        when(calcServ.substract(10.0, 5.0)).thenReturn(5.0);
        Assert.assertEquals(myApplication.substract(10.0, 5.0), 5.0);
    }

}
