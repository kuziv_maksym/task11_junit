package com.epam;

import com.epam.model.Task1;

import java.util.Arrays;

public class Application {
    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 3, 3, 4, 5, 5, 5, 6, 9, 10, 14};
        System.out.println(Arrays.toString(new Task1().array(arr)));
    }
}
