package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
public class Task1 {
    public static Logger logger = LogManager.getLogger(Task1.class);

    public  int[] array(int[] arr) {
        int[] result = new int[arr.length];
        int count = 0;
        int num = 0;
        for (int i = 0; i < arr.length-1; i++) {
            if (i == 0) {
                if (arr[i] == arr[i + 1]) {
                    result[count] = arr[i];
                    count++;
                }
            } else {
                if (arr[i] == arr[i+1] || arr[i] == arr[i-1]) {
                    result[count] = arr[i];
                    count++;
                }
            }
        }
        int[] res = new int[count];
        for (int i = 0; i < res.length; i++){
            res[i] = result[i];
        }
        logger.info("It's OK!");
        return res;
    }
}
