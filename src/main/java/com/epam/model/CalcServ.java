package com.epam.model;

public interface CalcServ {
    public double substract(double d1, double d2);
    public double add(double d1, double d2);
}
