package com.epam.model;

public class MyApplication {
     CalcServ calcServ;

    public MyApplication(CalcServ calcServ) {
        this.calcServ = calcServ;

    }

    public void setCalcServ(CalcServ calcServ) {
        this.calcServ = calcServ;
    }

    public double add(double d1, double d2) {
     double res = calcServ.add(d1,d2);
     return res;
    }


 public double substract(double d1, double d2) {
  double res = calcServ.substract(d1,d2);
  return res;
 }

}
